use std::error::Error;
use std::fmt::{
    Debug, Display,
};
use std::sync::mpsc::SendError;

use async_std::future::TimeoutError;
use zbus_lib::err::ZbusErr;

pub type FrameResult<T> = Result<T, ZbusFrameworkErr>;
pub type OkResult = FrameResult<()>;

pub struct ZbusFrameworkErr {
    inner: ErrKind,
}

impl ZbusFrameworkErr {
    pub fn new(inner: ErrKind) -> Self {
        ZbusFrameworkErr {
            inner
        }
    }
    pub fn err<E: Into<String>>(err: E) -> Self {
        Self::new(ErrKind::Err(From::from(err.into())))
    }
    pub fn rpc(err: ZbusErr) -> Self {
        Self::new(
            ErrKind::RpcErr(err)
        )
    }
    pub fn format<E: Into<String>>(err: E) -> Self {
        Self::new(
            ErrKind::FormatErr(err.into())
        )
    }
}

impl From<ZbusErr> for ZbusFrameworkErr {
    fn from(err: ZbusErr) -> Self {
        Self {
            inner: ErrKind::RpcErr(err),
        }
    }
}


// impl From<zip::result::ZipError> for SystemErr {
//     fn from(err: zip::result::ZipError) -> Self {
//         Self::err(err.to_string())
//     }
// }

impl From<TimeoutError> for ZbusFrameworkErr {
    fn from(err: TimeoutError) -> Self {
        Self::err(err.to_string())
    }
}
 
// pub enum ZbusErr {
//     TimeOut(String),
//     Closed,
//     Err(String),
//     Validate(String),


// impl From<RecvTimeoutError> for ZbusErr {
//     fn from(err: RecvTimeoutError) -> Self {
//         ZbusErr::new(ErrKind::TimeOut(err.to_string(), -1))
//     }
// }


impl<B> From<Box<B>> for ZbusFrameworkErr
    where
        B: Error + Send + Sync + 'static,
{
    fn from(err: Box<B>) -> ZbusFrameworkErr {
        ZbusFrameworkErr::new(ErrKind::Err(err))
    }
}

impl<T> From<SendError<T>> for ZbusFrameworkErr

{
    fn from(err: SendError<T>) -> ZbusFrameworkErr {
        ZbusFrameworkErr::err(err.to_string())
    }
}

impl From<serde_json::error::Error> for ZbusFrameworkErr

{
    fn from(err: serde_json::error::Error) -> ZbusFrameworkErr {
        ZbusFrameworkErr::format(err.to_string())
    }
}

impl Debug for ZbusFrameworkErr {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match &self.inner {
            ErrKind::RpcErr(ref msg) => write!(f, "validate fail {}", msg),
            ErrKind::FormatErr(ref msg) => write!(f, "validate fail {}", msg),
            ErrKind::Err(err) => write!(f, "{}", err.to_string()),
        }
    }
}

impl Error for ZbusFrameworkErr {

}
impl Display for ZbusFrameworkErr {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match &self.inner {
            ErrKind::RpcErr(ref msg) => write!(f, "validate fail {}", msg),
            ErrKind::FormatErr(ref msg) => write!(f, "validate fail {}", msg),
            ErrKind::Err(err) => write!(f, "{}", err.to_string()),
        }
    }
}

pub enum ErrKind {
    FormatErr(String),
    //格式错误
    //包装错误，这个需要自定义了，根据开发过程中
    RpcErr(ZbusErr),
    Err(Box<dyn  Error + Send + Sync>),//显示错误信息，而不需要处理的错误
}

