use serde_json::Value;
use zbus_lib::err::ZbusResult;

mod frame;
mod common;
mod app;
mod utils;

#[cfg(test)]
mod tests {
    use zbus_lib::rpc::IOHandlers;
    use serde_json::Value;
    use crate::extract_face_feature;
    use zbus_lib::service::WsRpcServer;
    use std::time::Duration;
    use crate::prelude::App;

    #[test]
    fn it_works() {

        let app = App;
        //rpc client
        // let rpc_client = WsRpcClient::connect("ws://42.51.4.33:15555");
        let n_workers = 10;
        let mut io_handler = IOHandlers::new(n_workers);
        // let rpc_client = WsRpcClient::connect("ws://127.0.0.1:15555");
        // ZApp::regist("rpcClient", rpc_client);
//注册rpc service
        io_handler.add_method("/face/faceCheck/extractFaceFeature".into(), move |mut params: Value| {
            Ok(if params.is_array() {// TODO 这块的代码都可以用宏来消防模版代码
                let params = params.as_array_mut().unwrap();
                params.reverse();
                let a = params.pop().unwrap();
                let a = serde_json::from_value(a).unwrap();
                let result = extract_face_feature(a);
                match result {
                    Ok(result) => { result }
                    Err(e) => { Value::Null }
                }
            } else {
                Value::Null
            })
        });
        // let rpc_server = WsRpcServer::connect("ws://10.51.4.33:15555", "/face".into(), io_handler);
        let rpc_server = WsRpcServer::connect("ws://127.0.0.1:15555", "/face".into(), io_handler);
        rpc_server.registry_service();
        println!("准备结束");
        use std::thread;
        thread::sleep(Duration::from_secs(100*10));
    }
}

///提取人脸特征
fn extract_face_feature(data: String) -> ZbusResult<Value> {
    Ok(Value::String("success".to_string()))
}


pub mod prelude {
    pub use crate::app::App;
    pub use crate::app::Component;
    pub use crate::frame::*;
    pub use crate::common::*;
}
