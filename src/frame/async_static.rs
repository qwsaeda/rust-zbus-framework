use std::future::Future;
use std::time::Duration;

use async_std::future;
use async_std::future::TimeoutError;

pub async fn timeout<F, T>(f: F) -> Result<T, TimeoutError>
    where
        F: Future<Output=T>,
{
    timeout_secs(f, 8).await
}

pub async fn timeout_secs<F, T>(f: F, seconds: u64) -> Result<T, TimeoutError>
    where
        F: Future<Output=T>,
{
    let dur = Duration::from_secs(seconds);
    future::timeout(dur, f).await
}

// pub async fn spawn_timeout<F, T>(f: F) -> Result<T, TimeoutError>
//     where
//         F: Future<Output=T>, {
//     task::spawn(async { timeout(f) }).await;
// }