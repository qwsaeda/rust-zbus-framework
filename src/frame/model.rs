use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct PageModel<E> {
    pageIndex: i32,
    // 当前页（第一页=1）
    recCount: i32,
    // 总记录数
    pageSize: i32,
    // 每页行数
    // String sortCol, // 排序字段
    // String sortOrder, // 排序方式
    data: Option<Vec<E>>,
    // 当前页的记录集
    pages: i32,
    // 总页数
    firstPage: bool,
    // 第一页？
    lastPage: bool,
    // 最后一页？
    hasNext: bool,
    // 是否有下一页？
    hasPrev: bool, // 是否有上一页？
}

impl<E> PageModel<E> {
    pub fn builder() -> PageModelBuilder<E> {
        PageModelBuilder {
            page_index: 1, // 当前页（第一页=1）
            rec_count: 0,  // 总记录数
            page_size: 15, // 每页行数
            // String sortCol, // 排序字段
            // String sortOrder, // 排序方式
            data: None, // 当前页的记录集

            pages: 0,          // 总页数
            first_page: false, // 第一页？
            last_page: false,  // 最后一页？
            has_next: false,   // 是否有下一页？
            has_prev: false,   // 是否有上一页？
        }
    }

    pub fn page_index(&self) -> i32 {
        self.pageIndex
    }
    // 当前页（第一页=1）
    pub fn rec_count(&self) -> i32 {
        self.recCount
    }
    // 总记录数
    pub fn page_size(&self) -> i32 {
        self.pageSize
    }
    pub fn data(&self) -> &Option<Vec<E>> {
        &self.data
    }

    pub fn pages(&self) -> i32 {
        self.pages
    }
    // 总页数
    pub fn first_page(&self) -> bool {
        self.firstPage
    }
    pub fn last_page(&self) -> bool {
        self.lastPage
    }
    pub fn has_next(&self) -> bool {
        self.hasNext
    }
    pub fn has_prev(&self) -> bool {
        self.hasPrev
    }
}

pub struct PageModelBuilder<E> {
    page_index: i32,
    // 当前页（第一页=1）
    rec_count: i32,
    // 总记录数s
    page_size: i32,
    // 每页行数
    // String sortCol, // 排序字段
    // String sortOrder, // 排序方式
    data: Option<Vec<E>>, // 当前页的记录集

    pages: i32,
    // 总页数
    first_page: bool,
    // 第一页？
    last_page: bool,
    // 最后一页？
    has_next: bool,
    // 是否有下一页？
    has_prev: bool, // 是否有上一页？
}

impl<E> PageModelBuilder<E> {
    pub fn build(&mut self) -> PageModel<E> {
        self.parse_page_info();
        let data = std::mem::replace(&mut self.data, None);
        self.data = None;
        PageModel {
            pageIndex: self.page_index,
            // 当前页（第一页=1）
            recCount: self.rec_count,
            // 总记录数
            pageSize: self.page_size,
            // 每页行数
            data, // 当前页的记录集

            pages: self.pages,
            // 总页数
            firstPage: self.first_page,
            // 第一页？
            lastPage: self.last_page,
            // 最后一页？
            hasNext: self.has_next,
            // 是否有下一页？
            hasPrev: self.has_prev, // 是否有上一页？
        }
    }
    fn parse_page_info(&mut self) {
        if self.page_index == 0 {
            self.page_index(1);
        }
        if self.page_index == 1 {
            self.first_page(true);
        }
        let pages = self.rec_count / self.page_size;
        self.pages(if self.rec_count % self.page_size == 0 {
            pages
        } else {
            pages + 1
        });
        if self.pages - self.page_index > 0 {
            self.has_next(true);
        }
        if self.page_index > 1 {
            self.has_prev(true);
        }
        if self.pages == self.page_index {
            self.last_page(true);
        }
    }
}

impl<E> PageModelBuilder<E> {
    pub fn page_index(&mut self, page_index: i32) {
        self.page_index = page_index;
    }
    // 当前页（第一页=1）
    pub fn rec_count(&mut self, rec_count: i32) {
        self.rec_count = rec_count;
    }
    // 总记录数
    pub fn page_size(&mut self, page_size: i32) {
        self.page_size = page_size;
    }
    pub fn data(&mut self, data: Option<Vec<E>>) {
        self.data = data;
    }

    pub fn pages(&mut self, pages: i32) {
        self.pages = if self.pages == 0 { 1 } else { pages };
    }
    // 总页数
    fn first_page(&mut self, first_page: bool) {
        self.first_page = first_page;
    }
    fn last_page(&mut self, last_page: bool) {
        self.last_page = last_page;
    }
    fn has_next(&mut self, has_next: bool) {
        self.has_next = has_next;
    }
    fn has_prev(&mut self, has_prev: bool) {
        self.has_prev = has_prev;
    }
}
