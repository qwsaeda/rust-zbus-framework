use std::collections::HashMap;

use serde::{Deserialize, Serialize};
use serde_json::Value;
use zbus_lib::client::{WsRpcClient, RpcClient};
use zbus_lib::message::{Request, Response};

use crate::common::err::{FrameResult, OkResult, ZbusFrameworkErr};
use crate::frame::{PageModel, PageRequest};
use crate::utils::uuid::generate_uuid;
use crate::prelude::Component;

pub trait BaseService<E> where
    E: Serialize + Clone, {
    fn get_by_id<S: Into<String>>(id: S) -> FrameResult<Option<E>>;

    fn deleteById<S: Into<String>>(id: S) -> OkResult;

    fn deletedById<S: Into<String>>(id: S) -> OkResult;

    fn save(entity: E) -> FrameResult<E>;

    fn update(entity: E) -> FrameResult<E>;

    fn find_all(search: Option<PageRequest>) -> FrameResult<Vec<E>>;

    fn find_page_all(search: PageRequest) -> FrameResult<PageModel<E>>;

    fn get_count(search: Option<PageRequest>) -> FrameResult<i32>;

    fn isExist<S: Into<String>>(prop_name: S, prop_value: Value) -> FrameResult<bool>;

    /**
     * 根据查询条件返回记录集(分页)
     *
     * @param pageRequest
     * @return
     */
    fn find_columns_page_all(condition: PageRequest) -> FrameResult<PageModel<E>>;

    fn find_columns_all(search: PageRequest) -> FrameResult<Vec<E>>;

    fn modify(entity: E) -> FrameResult<E>;

    fn create(entity: E) -> FrameResult<E>;

    fn get_by_kv(key: String, value: Value) -> FrameResult<Option<E>> {
        Self::find_all(Some(PageRequest::And_equal(key, value))).and_then(|es| Ok(if es.len() == 0 { Some(es[0].clone()) } else { None }))
    }

    fn get_by_condition(condition: PageRequest) -> FrameResult<Option<E>> {
        Self::find_all(Some(condition)).and_then(|es| Ok(if es.len() == 0 { Some(es[0].clone()) } else { None }))
    }
}

pub trait RpcService<E>
    where
        E: Serialize + for<'de> Deserialize<'de> + Clone,
{
    fn get_by_id<S: Into<String>>(id: S) -> FrameResult<Option<E>> {
        return Self::invoke("getById", Value::String(id.into()));
    }

    fn request_rpc_url<S: Into<String>>(method: S) -> String {
        let mut req_path = Self::mq_path() + "/" + &Self::service();
        if let Some(version) = Self::version() {
            req_path = req_path + "/" + &version;
        };
        req_path + "/" + &method.into()
    }

    fn mq_path() -> String;
    fn service() -> String;
    fn version() -> Option<String>;
    fn current_user() -> Option<HashMap<String, String>> {
        Some(HashMap::new())
    }//框架层面来统一实现，service可以重写实现
    // /**
    //  * @param method 方法名
    //  * @param params 多个参数是ojbect数组
    //  * @return 返回对象是实体 @
    //  */
    // fn invoke_value<S: Into<String>>(method: S, params: Value) -> FrameResult<Option<E>> {
    //     Self::invoke(method.into(), params)
    // }
    // // TODO 这两个invoke方法可能不需要存在，因为rust解析json库
    // /**
    //  * @param method 方法名
    //  * @param params 多个参数是ojbect数组
    //  * @return 返回对象是Vec
    //  */
    // fn invoke_vec<S: Into<String>>(method: S, params: Value) -> FrameResult<Vec<E>> {
    //     Self::invoke(method.into(), params)
    // }

    fn invoke<T: for<'de> Deserialize<'de>, S: Into<String>>(method: S, params: Value) -> FrameResult<T> {
        let request = request(Self::request_rpc_url(method), Value::Array(vec![params]));
        call(request).and_then(|resp|

            if resp.status() == 200 {
                serde_json::from_str(&resp.body().to_string()).map_err(|e| From::from(e))
            } else {
                Err(ZbusFrameworkErr::err(resp.body().to_string()))
            })
    }
    fn deliver<T: for<'de> Deserialize<'de>>(request: Request) -> FrameResult<T> {// 明天来写

        call(request).and_then(|resp|

            if resp.status() == 200 {
                serde_json::from_str(&resp.body().to_string()).map_err(|e| From::from(e))
            } else {
                Err(ZbusFrameworkErr::err(resp.body().to_string()))
            })
    }

    fn delete_by_id<S: Into<String>>(id: S) -> OkResult {
        Self::invoke("deleteById", Value::String(id.into()))
    }

    fn deleted_by_id<S: Into<String>>(id: S) -> OkResult {
        Self::invoke("deletedById", Value::String(id.into()))
    }

    fn save(entity: E) -> OkResult {
        let p = serde_json::to_value(entity)?;
        Self::invoke("save", p).map(|_: ()| ())
    }

    fn update(entity: E) -> OkResult {
        let p = serde_json::to_value(entity)?;
        Self::invoke("update", p).map(|_: ()| ())
    }

    fn find_all(search: Option<PageRequest>) -> FrameResult<Vec<E>> {
        match search {
            Some(search) => {
                let search = serde_json::to_value(search)?;
                Self::invoke("findAll", search)
            }
            None => Self::invoke("findAll", Value::Null),
        }
    }

    fn find_page_all(search: PageRequest) -> FrameResult<PageModel<E>> {
        let search = serde_json::to_value(search)?;
        Self::invoke("findPageAll", search)
    }

    fn get_count(search: Option<PageRequest>) -> FrameResult<i32> {
        match search {
            Some(search) => {
                let search = serde_json::to_value(search)?;
                Self::invoke("getCount", search)
            }
            None => Self::invoke("getCount", Value::Null),
        }
    }

    fn isExist<S: Into<String>>(prop_name: S, prop_value: Value) -> FrameResult<bool> {
        Self::invoke(
            "isExist",
            Value::Array(vec![Value::String(prop_name.into()), prop_value]),
        )
    }

    /**
     * 根据查询条件返回记录集(分页)
     *
     * @param pageRequest
     * @return
     */
    fn find_columns_page_all(pageRequest: PageRequest) -> FrameResult<PageModel<E>> {
        let search = serde_json::to_value(pageRequest)?;
        Self::invoke("findColumnsPageAll", search)
        // JSONValueToEntity(pm);
    }

    /**
     * 根据查询条件返回记录集
     *
     * @param pageRequest
     * @return
     */

    fn find_columns_all(search: PageRequest) -> FrameResult<Vec<E>> {
        let search = serde_json::to_value(search)?;
        Self::invoke("findColumnsAll", search)
    }

    fn modify(entity: E) -> FrameResult<E> {
        let p = serde_json::to_value(entity)?;
        Self::invoke("modify", p)
    }

    fn create(entity: E) -> FrameResult<E> {
        let p = serde_json::to_value(entity)?;
        Self::invoke("create", p)
    }

    fn get_by_kv(key: String, value: Value) -> FrameResult<Option<E>> {
        Self::find_all(Some(PageRequest::And_equal(key, value))).and_then(|es| Ok(if es.len() > 0 { Some(es[0].clone()) } else { None }))
    }

    fn get_by_condition(condition: PageRequest) -> FrameResult<Option<E>> {
        Self::find_all(Some(condition)).and_then(|es| Ok(if es.len() > 0 { Some(es[0].clone()) } else { None }))
    }
}

pub fn request_cookie<S: Into<String>>(url: S, params: Value, cookies: HashMap<String, String>) -> Request {
    Request::builder()
        .url(url.into())
        .method("GET")
        .body(params)
        .cookies(cookies)
        .id(generate_uuid())
        .build()
}

// fn parse<T: for<'de> Deserialize<'de>>(value: Value) -> FrameResult<T> {
//     serde_json::from_value(resp.body()).map_err(|e|From::from(e))
// }

pub fn request<S: Into<String>>(url: S, params: Value) -> Request {
    Request::builder()
        .url(url.into())
        .method("GET")
        .body(params)
        .id(generate_uuid())
        .build()
}

/**
 * 覆写是为了插入用户信息
 */
pub fn call(req: Request) -> FrameResult<Response> {
    WsRpcClient::component().map_or_else(|| Err(ZbusFrameworkErr::err("WsRpcClient is not regist")), |rpc_client| rpc_client.deliver(req).map_err(|e| From::from(e)))
    //发送数据，投递成功，再根据Id去查询结果，最后返回结果或错误
    // super::call(req, Value::Null) //覆写，然后调用父类

    //获取 rpchandler,然后发送请求，等待响应(目前用的都是线程并发，后面改为异步，异步相关的组件大部分都要改，尤其是等待方法的)
}
