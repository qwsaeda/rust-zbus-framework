use std::collections::HashMap;

use itertools::Itertools;
use serde::{Deserialize, Serialize};
use serde::export::fmt::Display;
use serde_json::Value;

#[derive(Serialize, Deserialize)]
pub struct PageRequest {
    // 请求页
    currentPage: i32,
    // 每页行数
    pageSize: i32,
    // 排序方式(asc desc)
    sortOrder: Option<String>,
    // 排序字段
    sortCol: Option<String>,
    // and条件
    andConditions: Vec<SearchCondition>,
    // or条件
    orConditions: Vec<SearchCondition>,
    // 多重排序
    sortConditions: Option<Vec<SearchCondition>>,
    map: Option<HashMap<String, Value>>, // 存放其他非string类型参数
}

impl PageRequest {
    const LEFT_SQL: &'static str = "leftSql";
    const MAP_COLUMNS: &'static str = "columns";
    const MAP_GROUP_BY: &'static str = "groupBy";
    const MAP_LEFT_SQL: &'static str = "leftSql";
    const ORDER_ASC: &'static str = "ASC";
    const ORDER_DESC: &'static str = "DESC";
    pub fn builder() -> PageRequestBuilder {
        PageRequestBuilder {
            currentPage: 1,
            pageSize: 15,
            sortOrder: None,
            sortCol: None,
            andConditions: Vec::new(),
            // or条件
            orConditions: Vec::new(),
            // 多重排序
            sortConditions: None,
            map: None, // 存放其他非string类型参数
        }
    }

    pub fn current_page(&self) -> i32 {
        self.currentPage
    }
    pub fn page_size(&self) -> i32 {
        self.pageSize
    }
    // pub fn   sortOrder: None,
    // pub fn   sortCol: None,
    pub fn and_conditions(&self) -> &Vec<SearchCondition> {
        &self.andConditions
    }
    // or条件
    pub fn or_conditions(&self) -> &Vec<SearchCondition> {
        &self.orConditions
    }

    // 多重排序
    pub fn sort_conditions(&self) -> &Option<Vec<SearchCondition>> {
        &self.sortConditions
    }
    pub fn map(&self) -> &Option<HashMap<String, Value>> {
        &self.map
    } // 存放其他非string类型参数

    pub fn put_map<S: Into<String>>(&mut self, key: S, value: Value) -> &mut Self {
        match self.map {
            Some(ref mut map) => { map.insert(key.into(), value); }
            None => {}
        };
        self
    }

    pub fn asc<S: Into<String>>(&mut self, col: S) -> &mut Self {
        self.insert_sort_condition(col, Self::ORDER_ASC)
    }

    pub fn desc<S: Into<String>>(&mut self, col: S) -> &mut Self {
        self.insert_sort_condition(col, Self::ORDER_DESC)
    }

    fn insert_sort_condition<S: Into<String>>(&mut self, col: S, order: &str) -> &mut Self {
        match self.sortConditions {
            Some(ref mut sortConditions) => {
                sortConditions.push(SearchCondition::new(
                    col.into(),
                    String::new(),
                    Value::String(order.into()),
                ));
            }
            None => {}
        };
        self
    }


    pub fn set_column<S: Into<String>>(&mut self, columns: S) -> &mut Self {
        self.set_columns(&vec![columns.into()])
    }

    pub fn set_columns<S: Into<String> + Display>(&mut self, columns: &[S]) -> &mut Self {
        let result = columns.into_iter().join(",");
        if !result.is_empty() {
            self.put_map(Self::MAP_COLUMNS, Value::String(result));
        }
        self
    }


    fn set_join_sql<S: Into<String>>(&mut self, keys: Vec<S>) -> &mut Self {
        let mut left_join_sql = String::new();
        for key in keys {
            left_join_sql = format!(" {}  {}", left_join_sql, key.into());
        }
        match &self.map {
            Some(map) if !left_join_sql.is_empty() => {
                map.get(Self::LEFT_SQL).map(|sql|
                    left_join_sql = format!(" {}  {}", sql.as_str().unwrap(), left_join_sql)
                );
                self.put_map(Self::LEFT_SQL, Value::String(left_join_sql));
            }
            _ => {}
        }
        self
    }


    pub fn left_join_sql<S: Into<String>>(&mut self, table: S, key: S, other_table_key: S) -> &mut Self {
        let left_join_sql = format!("LEFT JOIN {}  on  {}={}", table.into(), key.into(), other_table_key.into());
        self.set_join_sql(vec![left_join_sql])
    }

    pub fn right_join_sql<S: Into<String>>(&mut self, table: S, key: S, other_table_key: S) -> &mut Self {
        let right_join_sql = format!("RIGHT JOIN {}  on  {}={}", table.into(), key.into(), other_table_key.into());
        self.set_join_sql(vec![right_join_sql])
    }
}

impl Default for PageRequest {
    fn default() -> Self {
        Self::builder().build()
    }
}

impl PageRequest {
    pub fn and_equal<S: Into<String>>(&mut self, key: S, value: Value) -> &mut Self {
        self.insert_and_condition(key, SearchCondition::EQUAL, value)
    }

    pub fn and_escape<S: Into<String>>(&mut self, key: S, value: Value) -> &mut Self {
        self.insert_and_condition(key, SearchCondition::ESCAPE, value)
    }

    pub fn and_greater<S: Into<String>>(&mut self, key: S, value: Value) -> &mut Self {
        self.insert_and_condition(key, SearchCondition::GREATER, value)
    }

    pub fn and_greater_equal<S: Into<String>>(&mut self, key: S, value: Value) -> &mut Self {
        self.insert_and_condition(key, SearchCondition::LESS_EQUAL, value)
    }

    pub fn and_less<S: Into<String>>(&mut self, key: S, value: Value) -> &mut Self {
        self.insert_and_condition(key, SearchCondition::LESS, value)
    }

    pub fn and_less_equal<S: Into<String>>(&mut self, key: S, value: Value) -> &mut Self {
        self.insert_and_condition(key, SearchCondition::LESS_EQUAL, value)
    }

    pub fn and_not_equals<S: Into<String> + Clone>(
        &mut self,
        key: S,
        values: &[Value],
    ) -> &mut Self {
        for value in values {
            self.and_not_equal(key.clone(), value.clone());
        }
        self
    }
    pub fn and_not_equal<S: Into<String> + Clone>(&mut self, key: S, value: Value) -> &mut Self {
        self.insert_and_condition(key.clone(), SearchCondition::NOT_EQUAL, value)
    }

    pub fn and_like<S: Into<String>>(&mut self, key: S, value: S) -> &mut Self {
        self.insert_and_condition(
            key,
            SearchCondition::LIKE,
            Value::String("%".to_owned() + &value.into() + "%"),
        )
    }
    fn insert_and_condition<S: Into<String>>(
        &mut self,
        key: S,
        operator: &str,
        value: Value,
    ) -> &mut Self {
        self.andConditions
            .push(SearchCondition::new(key.into(), operator.into(), value));
        self
    }

    pub fn and_not_likes<S: Into<String> + Clone>(
        &mut self,
        key: S,
        mut keys: Vec<S>,
    ) -> &mut Self {
        let len = keys.len();
        let value = keys.remove(len - 1); // 取最后一个key做value,把第一个key放入数组尾部
        keys[len - 1] = key;
        for k in keys {
            self.and_not_like(k, value.clone());
        }
        self
    }
    pub fn and_not_like<S: Into<String> + Clone>(&mut self, key: S, value: S) -> &mut Self {
        self.insert_and_condition(
            key,
            SearchCondition::NOT_LIKE,
            Value::String("%".to_owned() + &value.into() + "%"),
        );
        self
    }
}

impl PageRequest {
    pub fn or_equal<S: Into<String>>(&mut self, key: S, value: Value) -> &mut Self {
        self.insert_or_condition(key, SearchCondition::EQUAL, value)
    }

    pub fn or_escape<S: Into<String>>(&mut self, key: S, value: Value) -> &mut Self {
        self.insert_or_condition(key, SearchCondition::ESCAPE, value)
    }

    pub fn or_greater<S: Into<String>>(&mut self, key: S, value: Value) -> &mut Self {
        self.insert_or_condition(key, SearchCondition::GREATER, value)
    }

    pub fn or_greater_equal<S: Into<String>>(&mut self, key: S, value: Value) -> &mut Self {
        self.insert_or_condition(key, SearchCondition::LESS_EQUAL, value)
    }

    pub fn or_less<S: Into<String>>(&mut self, key: S, value: Value) -> &mut Self {
        self.insert_or_condition(key, SearchCondition::LESS, value)
    }

    pub fn or_less_equal<S: Into<String>>(&mut self, key: S, value: Value) -> &mut Self {
        self.insert_or_condition(key, SearchCondition::LESS_EQUAL, value)
    }

    pub fn or_not_equals<S: Into<String> + Clone>(
        &mut self,
        key: S,
        values: &[Value],
    ) -> &mut Self {
        for value in values {
            self.or_not_equal(key.clone(), value.clone());
        }
        self
    }
    pub fn or_not_equal<S: Into<String> + Clone>(&mut self, key: S, value: Value) -> &mut Self {
        self.insert_or_condition(key.clone(), SearchCondition::NOT_EQUAL, value)
    }

    pub fn or_like<S: Into<String>>(&mut self, key: S, value: S) -> &mut Self {
        self.insert_or_condition(
            key,
            SearchCondition::LIKE,
            Value::String("%".to_owned() + &value.into() + "%"),
        );
        self
    }
    fn insert_or_condition<S: Into<String>>(
        &mut self,
        key: S,
        operator: &str,
        value: Value,
    ) -> &mut Self {
        self.orConditions
            .push(SearchCondition::new(key.into(), operator.into(), value));
        self
    }

    pub fn or_not_like<S: Into<String> + Clone>(&mut self, key: S, value: S) -> &mut Self {
        self.insert_or_condition(
            key,
            SearchCondition::NOT_LIKE,
            Value::String("%".to_owned() + &value.into() + "%"),
        )
    }
}

/// 静态And方法
impl PageRequest {
    pub fn And_equal<S: Into<String>>(key: S, value: Value) -> Self {
        let mut this = Self::default();
        this.and_equal(key, value);
        this
    }

    pub fn And_escape<S: Into<String>>(key: S, value: Value) -> Self {
        let mut this = Self::default();
        this.and_escape(key, value);
        this
    }

    pub fn And_greater<S: Into<String>>(key: S, value: Value) -> Self {
        let mut this = Self::default();
        this.and_greater(key, value);
        this
    }

    pub fn And_greater_equal<S: Into<String>>(key: S, value: Value) -> Self {
        let mut this = Self::default();
        this.and_greater_equal(key, value);
        this
    }

    pub fn And_less<S: Into<String>>(key: S, value: Value) -> Self {
        let mut this = Self::default();
        this.and_less(key, value);
        this
    }

    pub fn And_less_equal<S: Into<String>>(key: S, value: Value) -> Self {
        let mut this = Self::default();
        this.and_less_equal(key, value);
        this
    }


    pub fn And_not_equal<S: Into<String> + Clone>(key: S, value: Value) -> Self {
        let mut this = Self::default();
        this.and_not_equal(key, value);
        this
    }

    pub fn And_like<S: Into<String>>(key: S, value: S) -> Self {
        let mut this = Self::default();
        this.and_like(key, value);
        this
    }
}

/// 静态or方法
impl PageRequest {
    pub fn Or_equal<S: Into<String>>(key: S, value: Value) -> Self {
        let mut this = Self::default();
        this.or_equal(key, value);
        this
    }

    pub fn Or_escape<S: Into<String>>(key: S, value: Value) -> Self {
        let mut this = Self::default();
        this.or_escape(key, value);
        this
    }

    pub fn Or_greater<S: Into<String>>(key: S, value: Value) -> Self {
        let mut this = Self::default();
        this.or_greater(key, value);
        this
    }

    pub fn Or_greater_equal<S: Into<String>>(key: S, value: Value) -> Self {
        let mut this = Self::default();
        this.or_greater_equal(key, value);
        this
    }

    pub fn Or_less<S: Into<String>>(key: S, value: Value) -> Self {
        let mut this = Self::default();
        this.or_less(key, value);
        this
    }

    pub fn Or_less_equal<S: Into<String>>(key: S, value: Value) -> Self {
        let mut this = Self::default();
        this.or_less_equal(key, value);
        this
    }


    pub fn Or_not_equal<S: Into<String> + Clone>(key: S, value: Value) -> Self {
        let mut this = Self::default();
        this.or_not_equal(key.clone(), value);
        this
    }

    pub fn Or_like<S: Into<String>>(key: S, value: S) -> Self {
        let mut this = Self::default();
        this.or_like(
            key, value,
            // Value::String(),
        );
        Self::default()
    }


    pub fn Or_not_like<S: Into<String> + Clone>(key: S, value: S) -> Self {
        let mut this = Self::default();
        this.or_not_like(key, value);
        this
    }
}

pub struct PageRequestBuilder {
    currentPage: i32,
    // 每页行数
    pageSize: i32,
    // 排序方式(asc desc)
    sortOrder: Option<String>,
    // 排序字段
    sortCol: Option<String>,
    // and条件
    andConditions: Vec<SearchCondition>,
    // or条件
    orConditions: Vec<SearchCondition>,
    // 多重排序
    sortConditions: Option<Vec<SearchCondition>>,
    map: Option<HashMap<String, Value>>, // 存放其他非string类型参数
}

impl PageRequestBuilder {
    pub fn current_page(&mut self, current_page: i32) -> &mut Self {
        if self.currentPage <= 0 {
            self.currentPage = 1
        } else {
            self.currentPage = current_page;
        };
        self
    }
    pub fn page_size(&mut self, page_size: i32) -> &mut Self {
        if self.currentPage <= 0 {
            self.pageSize = 14
        } else {
            self.pageSize = page_size;
        };
        self
    }
    // pub fn   sortOrder: None,
    // pub fn   sortCol: None,
    pub fn and_conditions(&mut self, and_conditions: Vec<SearchCondition>) -> &mut Self {
        self.andConditions = and_conditions;
        self
    }
    // or条件
    pub fn or_conditions(&mut self, or_conditions: Vec<SearchCondition>) -> &mut Self {
        self.orConditions = or_conditions;
        self
    }

    // 多重排序
    pub fn sort_conditions(&mut self, sort_conditions: Option<Vec<SearchCondition>>) -> &mut Self {
        self.sortConditions = sort_conditions;
        self
    }
    pub fn map(&mut self, map: Option<HashMap<String, Value>>) -> &mut Self {
        self.map = map;
        self
    } // 存放其他非string类型参数

    pub fn build(&self) -> PageRequest {
        PageRequest {
            currentPage: self.currentPage,
            pageSize: self.pageSize,
            sortOrder: self.sortOrder.clone(),
            sortCol: self.sortCol.clone(),
            andConditions: self.andConditions.clone(),
            // or条件
            orConditions: self.orConditions.clone(),
            // 多重排序
            sortConditions: self.sortConditions.clone(),
            map: self.map.clone(), // 存放其他非string类型参数
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct SearchCondition {
    key: String,
    value: Value,
    operator: String,
}

impl SearchCondition {
    const GREATER: &'static str = ">";
    const LESS: &'static str = "<";
    const EQUAL: &'static str = "=";
    const NOT_EQUAL: &'static str = "<>";
    const GREATER_EQUAL: &'static str = ">=";
    const LESS_EQUAL: &'static str = "<=";
    const LIKE: &'static str = "like";
    const NOT_LIKE: &'static str = "not like";
    const ESCAPE: &'static str = "escape";
    fn new(key: String, operator: String, value: Value) -> Self {
        SearchCondition {
            key,
            value,
            operator,
        }
    }
}
