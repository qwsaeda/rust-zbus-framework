use std::fmt::Write;

use serde_json::Value;

/// 扩展一些快捷方法
// impl<'a> From<&'a String> for Value {
//     fn from(v: &'a str) -> Self {
//         Value::from(v)
//     }
// }

pub trait ToValue where Self: Into<String> {
    fn value(&self) -> Value;
}

impl ToValue for String {
    fn value(&self) -> Value {
        let val: String = self.into();
        Value::from(val)
    }
}

// impl<'a> Add for &'a str {
//     type Output = String;
//
//     fn add(self, rhs: Self) -> Self::Output {
//         format!("{}{}", self, self)
//     }
// }

#[test]
fn test_ids() {
    let ids = vec!["dadsf", "147411", "147411", "147411", "147411", "147411"];
    let result = ids.into_iter().to_ids("'", "','", "'");
    println!("{}", result);
}


pub trait ToIds: Iterator {
    fn to_ids(&mut self, prefix: &str, sep: &str, suffix: &str) -> String
        where Self::Item: std::fmt::Display
    {
        match self.next() {
            None => String::new(),
            Some(first_elt) => {
                // estimate lower bound of capacity needed
                let (lower, _) = self.size_hint();
                let mut result = String::with_capacity(sep.len() * lower);
                write!(&mut result, "{}{}", prefix, first_elt).unwrap();
                for elt in self {
                    result.push_str(sep);
                    write!(&mut result, "{}", elt).unwrap();
                }
                write!(&mut result, "{}", suffix).unwrap();
                result
            }
        }
    }
}


impl<T: ?Sized> ToIds for T where T: Iterator {}
