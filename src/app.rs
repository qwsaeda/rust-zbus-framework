use std::any::{Any, TypeId};
use std::collections::HashMap;
use std::sync::Mutex;
use zbus_lib::client::WsRpcClient;
use zbus_lib::rpc::WsRpcHandler;

lazy_static::lazy_static! {
    static  ref COMPONENTS : Mutex<HashMap<TypeId, Parts>>  = {
        Mutex::new(HashMap::new())
    };
}
pub trait Component<H>: Send + Sized + 'static where H: Send + Sized + 'static {
    fn init(&mut self) {}
    //通过线程ID注册，只运行一个app
    fn stop(&mut self) {}
    fn handler(&self) -> H;
    fn component() -> Option<Box<H>> {
        //参考actix使用thread_loal的全局变量的方案
        let components = COMPONENTS.lock().unwrap(); //锁的性能问题,改为读写锁，后面还可以改为线程变量方案
        if let Some(parts) = components.get(&TypeId::of::<Self>()) {
            if let Some(component) = parts.this.downcast_ref::<Self>() {
                return Some(Box::new(component.handler()));
            }
        };
        None
    }
}

struct Parts {
    pub name: String,
    pub this: Box<dyn Any + Send>,
}


pub struct App;

impl App {
    pub fn init(&self) {
        // component call init  循环
    }
    pub fn start() {
        let mut components = COMPONENTS.lock();
    }
    pub fn regist_component<T, H>(type_id: TypeId, name: String, component: T) where T: Send + Sized + Component<H> + 'static, H: Send + Sized + 'static {
        // println!("{:?}",)
        let mut components = COMPONENTS.lock().unwrap();
        let componet = Parts { name, this: Box::new(component) };
        components.insert(type_id, componet);
    }
    pub fn regist<S: Into<String>, T, H>(name: S, component: T) where T: Send + Sized + Component<H> + 'static, H: Send + Sized + 'static {
        // println!("{:?}",)
        Self::regist_component(Any::type_id(&component), name.into(), component)
    }
}
impl Component<WsRpcHandler> for WsRpcClient {
    fn handler(&self) -> WsRpcHandler {
        self.handler()
    }
}

