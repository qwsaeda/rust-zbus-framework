use uid::Uuid;

pub fn generate_uuid() -> String {
    let my_uuid = Uuid::new_v4();
    format!("{}", my_uuid).replacen("-", "", 4)
}